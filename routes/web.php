<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/decrypt', 'DecryptController@index')->name('decrypt-view');
Route::post('/decrypt', 'DecryptController@decrypt')->name('decrypt-process');


Route::get('/encrypt', 'EncryptController@index')->name('encrypt-view');
Route::post('/encrypt', 'EncryptController@encrypt')->name('encrypt-process');