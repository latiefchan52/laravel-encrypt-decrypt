<?php

namespace App\Helpers;

class StringHelper
{
    /**
     * Generate fake string 
     *     
     * @return String 
     */
    public function generateFakeString()
    {
        $randomKey = str_random(32) .date('YmdHis');
        $base64RandomKey = base64_encode($randomKey);
        $base64RandomKeyRandomAgain = base64_encode($base64RandomKey);
        return $base64RandomKey .$randomKey .$base64RandomKeyRandomAgain .date('YmdHis');
    }
}
