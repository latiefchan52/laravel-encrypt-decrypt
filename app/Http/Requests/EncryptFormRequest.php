<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncryptFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'teexte-file' => 'required|file|mimes:txt|max:1000',
            'key' => 'required'
        ];
    }
}
