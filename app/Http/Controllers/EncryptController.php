<?php

namespace App\Http\Controllers;

use File;
use Redirect;
use Response;
use Illuminate\Http\Request;
use App\Http\Requests\EncryptFormRequest;

class EncryptController extends Controller
{
    public function index()
    {
        return view('encrypt');
    }

    public function encrypt(EncryptFormRequest $request)
    {
        $textInFile = File::get($request->file('teexte-file'));
        $fileName = (is_null($request->input('filename'))) ? 'resultencrypt.txt' : $request->input('filename') .'.txt';
        $keyPhrase = $request->input('key');

        // check exist volume in file 
        $checkExistVolumeInFile = function () use ($textInFile) {
            $text = trim($textInFile);
            return ((is_null($textInFile) || '' == $text || empty($text))) ? false : true;                
        };

        // redirect with error message
        if (false == $checkExistVolumeInFile()) {
            return Redirect::back()->withErrors(['File Contents Cannot Be Blank']);
        }
        
        // encrypt text        
        $newEncrypter = new \Illuminate\Encryption\Encrypter(md5($keyPhrase), config('app.cipher'));
        $encryptText = $newEncrypter->encrypt($textInFile);

        return Response::make($encryptText, '200', array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' .$fileName .'"'
        ));
    }
}
