<?php

namespace App\Http\Controllers;

use File;
use Redirect;
use Response;
use Illuminate\Http\Request;
use App\Http\Requests\EncryptFormRequest;
use Illuminate\Contracts\Encryption\DecryptException;

class DecryptController extends Controller
{
    public function index()
    {
        return view('decrypt');
    }

    public function decrypt(EncryptFormRequest $request)
    {
        $textInFile = File::get($request->file('teexte-file'));
        $fileName = (is_null($request->input('filename'))) ? 'resultdecrypt.txt' : $request->input('filename') .'.txt';
        $keyPhrase = $request->input('key');

        // check exist volume in file 
        $checkExistVolumeInFile = function () use ($textInFile) {
            $text = trim($textInFile);
            return ((is_null($textInFile) || '' == $text || empty($text))) ? false : true;                
        };

        // redirect with error message
        if (false == $checkExistVolumeInFile()) {
            return Redirect::back()->withErrors(['File Contents Cannot Be Blank']);
        }

        // decrypt text
        $newEncrypter = new \Illuminate\Encryption\Encrypter(md5($keyPhrase), config('app.cipher'));

        try {
            $decryptText = $newEncrypter->decrypt($textInFile);
        } catch (DecryptException $e) {
            $decryptText = app('string.helper')->generateFakeString();
        }

        return Response::make($decryptText, '200', array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' .$fileName .'"'
        ));
    }
}
