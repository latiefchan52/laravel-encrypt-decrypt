<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">        

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>        
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Encrypt
                </div>
                {!! Form::open(['route' => 'encrypt-process', 'files' => true, 'class' => 'form-inline center-block well']) !!}                
                    <div class="input-group">
                        <label id="browsebutton" class="btn btn-default input-group-addon" for="my-file-selector" style="background-color:white">
                            <input id="my-file-selector" name="teexte-file" type="file" style="display:none;">Browse...
                        </label>
                        <input id="label" type="text" class="form-control" readonly>
                    </div>
                    <div class="input-group">
                        <input id="label" type="text" name="key" placeholder="key" class="form-control" required>
                    </div>
                    <div class="input-group">
                        <input id="label" type="text" name="filename" placeholder="file name" class="form-control" required>
                    </div>
                    <span class="help-block"></span>
                    <button type="submit" class="btn btn-primary">Upload</button>
                    <span class="help-block">
                        <small id="fileHelp" class="form-text text-muted">Only txt with size less than 1MB is allowed.</small>
                    </span>
                    <div class="links">                        
                        <a href="{{ url('/decrypt') }}">Go To Decrypt</a>
                    </div>
                    <ul>
                        @foreach($errors->all() as $error)
                            <span class="help-block">
                                <li><strong>{{ $error }}</strong></li>
                            </span>
                        @endforeach
                    </ul>
                {!! Form::close() !!}
            </div>            
        </div>         
    </body>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#browsebutton :file').change(function(e){
                var fileName = e.target.files[0].name;
                $("#label").attr('placeholder',fileName)
            });
        });
    </script>
</html>
